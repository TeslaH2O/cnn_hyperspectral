# Spectral-spatial classification of hyperspectral images: three tricks and a new supervised learning setting

>This repository contains a Python implementantion of a CNN using the
Nolearn/Lasagne/Theano libraries. 
>You can re-run the experiments on the provided hyperpectral image 
datasets or load the best network configuration for each dataset.
>TODO allow the user to provide extra datasets

# How to setup your system
>First of all clone the repository into a directory of your choice.
>To run the cnn.py Python script inside the cnn directory you need
to follow the simple steps below.

# Required packages
>You need your machine to have working C/C++ and Fortran compilers.
Moreover you will need:

    gnuplot, openblas, python-dev
    
>properly installed and working on your machine
>If your system is Ubuntu/Debian based you can easily install all of them
with this command

    sudo apt-get install build-essential gnuplot gfortran libfreetype6-dev libopenblas-dev python-dev


# Use Virtualenv
>Before installing python dependencies I advise you to use virtualenv 
(https://virtualenv.readthedocs.org/en/latest/) to avoid to mess up 
with your sistem libraries.
>If you don't have virtualenv installed follow the installation guide
available on the virtualenv site.
>To setup a virtual enviroment you just need to run this command:

    virtualenv venv

>And then to use the virtual environment

    source venv/bin/activate

>To deactivate the environment simply run

    deactivate

# Install required Python packages
>For running this script you need to have an updated version of Pip
installed (See https://pip.pypa.io/en/stable/ for how to install it).

### Linux / Mac OS X systems
>In order to use my code you have just to run the setup.sh 
with the command from:

    bash setup.sh

### Windows sistems
>Here https://sites.google.com/site/pydatalog/python/pip-for-windows 
you will find a good starting point for using pip and virtualenv on 
Windows once you configure Python for Windows properly.
>There is a non tested setup.bat similar to the Linux one. Take it
as a descriptive file for the steps to follow to install
the required dependencies and source files.
    

# Running the script
>If nothing went wrong you can already run the main script file
that implements a Convolutional Neural Network and it is already
set up for loading the datasets considered in the paper.

### Replicating results from the paper
>By executing the script 'replicate_paper_results.sh' with the command:

    bash replicate_paper_results.sh

>you will be able to replicate the results shown in Table IV of the paper. 
>These results were achieved by using a limited number of training samples
>for each class (max 200 samples for each class).

### Main options

>To run the Python script file from the main directory:

    python cnn/cnn.py
    

>Then, to select the dataset you can use the option:

    --dataset=DATASET
>where possible values for DATASET are

    [PAVIA,PAVIAU,KSC,PINE,SALINA]
              
>To define the spatial preprocessing you can use the option:

    --window=<type><size>
    
>where the available 'type's of smoothing window are

    [gaussian,triangular,mean]

>and the 'size' must be an odd integer greater then 0 (size=1 means no smoothing)

>To build a reduced train set and do data augmentation on it you can use the option:

    --balance_train=[a][l]n
    
>where 'a' is optional and indicates to use noise-based data augmentation, 
'l' is optional and indicates to use label-based data augmentation and 'n' is the 
>maximum number of samples for each class to include in the train set.
        
>To load the best trained network you have to use in addition
the option:

    --load_best_network

>Instead to load the last trained network with grid search you have to use 
the option:

    --load_network

>Networks trained without grid search need to be renamed adding the prefix

    last_

>and then use the option --load_network.
>If you also want to see the value of the main parameters of the network
you have to use also

    --short_print_network

>Instead if you want to train a new network you have to add only the 
option:

    --hyperparameter_optimization=SEARCH_METHOD
    
>where the possible search methods are

    [RANDOM,EXHAUSTIVE]
    
>After selecting the dataset to use with --dataset=DATASET_NAME
>For the full list of the script options use

    --help

### Use the algorithm on your own data
>To include you dataset you just have to create a sub-directory in the
>'dataset' directory and add an entry in the function 'load_data' inside
>the 'utils.py' file. There you have to add another case in the 'if elif else'
>construct, taking as example the already provided code for the paper datasets.
>N.B. The dataset files must be in the Matlab .mat format and the ground-truth
>file is assume to have the same name as the dataset file. In addition to this,
>it is supposed to have the '_gt' suffix in the file name and in the variable name
>inside the .mat file.