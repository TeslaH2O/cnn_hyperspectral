#!/usr/bin/env/python
# System libraries
import sys,os,glob
import multiprocessing
# Numpy, Theano, Lasagne, Nolearn, Sklearn, Scipy
import numpy as np
import theano
import lasagne
from lasagne import layers
from lasagne.updates import nesterov_momentum
from lasagne.objectives import categorical_crossentropy
from nolearn.lasagne import BatchIterator, TrainSplit
from nolearn.lasagne import NeuralNet
from sklearn.metrics import classification_report, accuracy_score, confusion_matrix
from lib.sklearn import grid_search
from scipy.stats import randint as sp_randint
from scipy.stats import uniform as sp_uniform

# Import argument parsing library
import argparse, argcomplete

# My own utils library
from utils import load_data, saveVar, loadVar, EarlyStopping, my_objective, printNet, float32

parser = argparse.ArgumentParser(description='CNN using Theano, Lasagne and NoLearn.')
parser.add_argument('--load_network',help='Load a previously saved network',action='store_true')
parser.add_argument('--load_best_network',help='Load the best network for the selected dataset',action='store_true')
parser.add_argument('--print_network',help='Print the loaded network',action='store_true')
parser.add_argument('--short_print_network',help='Print the main parameters of the network',action='store_true')
parser.add_argument('--test_seed_initialization',help='Test for the correct seed initialization',action='store_true')
parser.add_argument('--network_type',help='The type of network to use if not loaded [CONVNET,LOGREG] (default CONVNET)')
parser.add_argument('--num_epochs',help='Max number of epochs for training (default 5000)')
parser.add_argument('--learning_rate',help='The learning rate (default 0.001)')
parser.add_argument('--kernel_size',help='The size of the kernel (default 87)')
parser.add_argument('--num_kernel',help='The number of kernels to use (default 2)')
parser.add_argument('--momentum',help='The momentum of the Nesterov Gradient Descend (default 0.7)')
parser.add_argument('--num_classes',help='Number of classes (deprecated)')
parser.add_argument('--batch_size',help='Batch size (default 1)')
parser.add_argument('--stride',help='The convolutional stride (default 33)')
parser.add_argument('--window',help='The smoothing window length (default 1 NO SMOOTHING)')
parser.add_argument('--best_window',help='Use the best smoothing window length (default False)',action='store_true')
parser.add_argument('--lamda1',help='The constant for the L2 norm regularization (default 0)')
parser.add_argument('--lamda2',help='The constant for the proximity L2 norm regularization (default 0)')
parser.add_argument('--earlystop',help='Use early stop (default false)',action='store_true')
parser.add_argument('--earlystop_iteration',help='The number of iteration to wait for Early-stop (default 100)')
parser.add_argument('--balance_train',help='Balance the train set class-wise (default: 0 means no balance)')

parser.add_argument('--dataset_seed',help='Seed to use to shuffle the datasets (default -1 means no shuffle)')
parser.add_argument('--lasagne_seed',help='Seed to use to initialize the weights of the network (-1 means no shuffle, default ranodom)')
parser.add_argument('--hyperparameter_optimization',help='Use hyperparameter optimiztion [NONE|RANDOM|EXHAUSTIVE] (default NONE)')
parser.add_argument('--dataset',help='The dataset to use (default extFTIR beer dataset)')
parser.add_argument('--max_cpus',help='Maximum number of cpus available (default is the half of the system cpus for security reasons)')

argcomplete.autocomplete(parser)
args = parser.parse_args()
# Network parameters
NETWORK_TYPE = args.network_type if args.network_type else 'CONVNET'
LOAD_NETWORK = bool(args.load_network) if args.load_network else False
PRINT_NETWORK = bool(args.print_network) if args.print_network else False
LOAD_BEST_NETWORK = bool(args.load_best_network) if args.load_best_network else False
SHORT_PRINT_NETWORK = bool(args.short_print_network) if args.short_print_network else False
TEST_SEED_INITIALIZATION = bool(args.test_seed_initialization) if args.test_seed_initialization else False
NUM_EPOCHS = int(args.num_epochs) if args.num_epochs else 50000
LEARNING_RATE = float(args.learning_rate) if args.learning_rate else 0.005
KERNEL_SIZE = int(args.kernel_size) if args.kernel_size else 51
STRIDE = int(args.stride) if args.stride else 1
NUM_KERNEL = int(args.num_kernel) if args.num_kernel else 16
MOMENTUM = float(args.momentum) if args.momentum else 0.8
SMOOTHING_WINDOW = args.window if args.window else 1


NUM_CLASSES = int(args.num_classes) if args.num_classes else 2
BATCH_SIZE = int(args.batch_size) if args.batch_size else 100
BATCH_SIZE_TEST = BATCH_SIZE

LAMDA1 = float(args.lamda1) if args.lamda1 else 1.0
LAMDA2 = float(args.lamda2) if args.lamda2 else 0

EARLYSTOP_ITER = int(args.earlystop_iteration) if args.earlystop_iteration else 100
EARLYSTOP=bool(args.earlystop) if args.earlystop else False
if args.earlystop_iteration:
    EARLYSTOP=True
    
# Parameters for samples' analysis

SEED = int(args.dataset_seed) if args.dataset_seed else 0 #seed = -1 means no dataset shuffling
LASAGNE_SEED = int(args.lasagne_seed) if args.lasagne_seed else np.random.randint(100)

BALANCE = args.balance_train if args.balance_train else 0

# Set it to:
#		'RANDOM' for random hyperparameter optimization with grid search
#		'EXHAUSTIVE' for exhaustive hyperparameter optimization with grid search
#		'NONE' to use the network parameters defined above
HYPERPARAM_OPT = args.hyperparameter_optimization if args.hyperparameter_optimization else 'NONE'
# Environment variable
DATASET = args.dataset if args.dataset else 'PINE'
NETWORK_NAME = ('best_' if LOAD_BEST_NETWORK else 'last_') + NETWORK_TYPE
ROOT_DIR = os.path.dirname(os.path.dirname(os.path.realpath(__file__))) + os.sep

OUTPUT_DIR = '.'

MAX_CPUS = int(args.max_cpus) if args.max_cpus else multiprocessing.cpu_count()

SAVED_VAR_DIR = ROOT_DIR + 'saved_networks' + os.sep  + DATASET  + os.sep

if LOAD_BEST_NETWORK:
    LOAD_NETWORK=True
    SMOOTHING_WINDOW='gaussian' + filter(str.isdigit, glob.glob(SAVED_VAR_DIR + NETWORK_NAME + '*.pickle')[0].rsplit(os.sep,1)[-1])
if args.best_window:
    SMOOTHING_WINDOW='gaussian' + filter(str.isdigit, glob.glob(SAVED_VAR_DIR + NETWORK_NAME + '*.pickle')[0].rsplit(os.sep,1)[-1])
# Loading dataset
X, y, X_test,y_test,_,scaler = load_data(ROOT_DIR,DATASET,scale_dataset=True,shuffle=SEED,balance=BALANCE,window=SMOOTHING_WINDOW,return_scaler=True,saved_scaler=LOAD_BEST_NETWORK)

NO_TEST_SET=(X_test.shape[0]==0)
# Num of classes
NUM_CLASSES=len(np.unique(y)) 
     
# Cross-validation folds
N_FOLDS = 10 

if BATCH_SIZE<0:
  BATCH_SIZE=y.shape[0]
  BATCH_SIZE_TEST=BATCH_SIZE

if not os.path.exists(SAVED_VAR_DIR):
    os.makedirs(SAVED_VAR_DIR)

if LASAGNE_SEED>0 and not LOAD_NETWORK and HYPERPARAM_OPT=='NONE':
  if not args.lasagne_seed:
    print('Using seed %d for initializing weights') % (LASAGNE_SEED)	
    raw_input('Press ENTER to continue...')
  lasagne.random.set_rng(np.random.RandomState(LASAGNE_SEED))

print('Train-valid set size:',X.shape[0],'Test set size:',X_test.shape[0],'#Classes:',NUM_CLASSES,'Batch size:',BATCH_SIZE,'#Features:',X.shape[1])


if LOAD_NETWORK:
    
    best_net=loadVar(NETWORK_NAME + ('_w' + str(SMOOTHING_WINDOW) if SMOOTHING_WINDOW>1 else ''),SAVED_VAR_DIR)
    if PRINT_NETWORK:
        printNet(best_net)
    elif SHORT_PRINT_NETWORK:
        params_=getCNNParams(NETWORK_NAME,SAVED_VAR_DIR)
        for key,value in params_.iteritems():
            try:
                print('%s\t=>\t%f')%(key.upper(),value)
            except:
                pass
    HYPERPARAM_OPT='NONE'
else:
    adjust_and_stop=[
        EarlyStopping(patience=EARLYSTOP_ITER)
    ]
    if EARLYSTOP and EARLYSTOP_ITER>0:
        adjust_and_stop.append(EarlyStopping(patience=EARLYSTOP_ITER))
    layers=[('input', layers.InputLayer),
                ('conv1d', layers.Conv1DLayer),

                ('output', layers.DenseLayer),
                ]
    net1 = NeuralNet(
            layers=layers,
            # input layer
            input_shape=(None, 1, X.shape[1]),
            # layer conv1d
            conv1d_num_filters=NUM_KERNEL,
            conv1d_filter_size= KERNEL_SIZE,
            conv1d_stride=STRIDE,
            conv1d_pad='same',
            conv1d_nonlinearity=lasagne.nonlinearities.rectify,
            conv1d_W=lasagne.init.HeNormal(gain='relu'),
            conv1d_b=lasagne.init.Constant(0.),
            # output layer
            output_nonlinearity=lasagne.nonlinearities.softmax,
            output_W=lasagne.init.GlorotUniform() ,
            output_b=lasagne.init.Constant(0.),
            output_num_units=NUM_CLASSES,
            # optimization method params
            objective=my_objective,
            objective_lamda1=LAMDA1,
            objective_lamda2=LAMDA2,
            update=nesterov_momentum,
            update_momentum=theano.shared(float32(MOMENTUM)),
            #update=sgd,
            train_split=TrainSplit(eval_size=0.2),
            update_learning_rate=theano.shared(float32(LEARNING_RATE)),
            max_epochs=NUM_EPOCHS,
            batch_iterator_train=BatchIterator(batch_size=BATCH_SIZE),
            batch_iterator_test=BatchIterator(batch_size=BATCH_SIZE_TEST),
            on_epoch_finished=[
                EarlyStopping(patience=EARLYSTOP_ITER),
                ],
            regression=False,
            seed=LASAGNE_SEED,
            verbose=(not HYPERPARAM_OPT=='RANDOM' and not HYPERPARAM_OPT=='EXHAUSTIVE'),
            )

      
# Test if the random initialization of weights is correctly working
if TEST_SEED_INITIALIZATION:
  print('Initialization of parameters is equal' if testNetworkInit(net1,LASAGNE_SEED) else 'Different initialization for parameters','with seed:',LASAGNE_SEED)
# Train the network and Hyperparameter optimization with GridSearchCV or RandomizedSearchCV
if HYPERPARAM_OPT=='RANDOM':

    param_dist={
                'conv1d_filter_size': np.random.choice(np.arange(start=5,stop=91,step=4),size=10),
                'on_epoch_finished_start' : [1e-4,1e-3, 1e-2, 1e-1],
                'update_momentum' : [0.1,0.3,0.5,0.7,0.9],
                'conv1d_num_filters' : [4,8,16,32],
                'conv1d_stride' : [1,2,3,4],
                'objective_lamda1' : [1e-4,1e-3,1e-2,1e-1,0,1,1e1,1e2,1e3,1e4],
                'objective_lamda2' : [1e-4,1e-3,1e-2,1e-1,0,1,1e1,1e2,1e3,1e4]



    }



    clf = grid_search.RandomizedSearchCV(net1, 
				      param_distributions=param_dist,
				      cv=N_FOLDS ,
				      scoring=None ,
				      n_jobs=int(MAX_CPUS),
				      refit=True,
				      n_iter=5*len(param_dist),verbose=10)
elif HYPERPARAM_OPT=='EXHAUSTIVE':

    parameters={
              'conv1d_filter_size': np.arange(start=5,stop=91,step=4),
	      'on_epoch_finished_start' : [1e-4,1e-3, 1e-2, 1e-1],
	      'update_momentum' : [0.1,0.3,0.5,0.7,0.9],
	      'conv1d_num_filters' : [4,8,16,32],
	      'conv1d_stride' : [1,2,3,4],
	      'objective_lamda1' : [1e-4,1e-3,1e-2,1e-1,0,1,1e1,1e2,1e3,1e4],
	      'objective_lamda2' : [1e-4,1e-3,1e-2,1e-1,0,1,1e1,1e2,1e3,1e4]
	   }

  
    clf = grid_search.GridSearchCV(net1, 
				parameters,
				cv=N_FOLDS,
				n_jobs=int(MAX_CPUS),verbose=10)
log_file=open(SAVED_VAR_DIR + NETWORK_NAME + '.log','w')
if HYPERPARAM_OPT=='RANDOM' or HYPERPARAM_OPT=='EXHAUSTIVE':
    clf.fit(X.reshape((-1, 1, X.shape[1])), y.astype(np.uint8))
    print('Train Accuracy:\t %.4f with parameters: \n')%(clf.best_score_)
    print(clf.grid_scores_)
    print(clf.best_params_)
    best_net=clf.best_estimator_
    saveVar(best_net,NETWORK_NAME + ('_w' + str(SMOOTHING_WINDOW) if SMOOTHING_WINDOW>1 else ''),SAVED_VAR_DIR)
    saveVar(clf.best_params_,NETWORK_NAME + '_params' + ('_w' + str(SMOOTHING_WINDOW) if SMOOTHING_WINDOW>1 else ''),SAVED_VAR_DIR)
    saveVar(clf.grid_scores_,NETWORK_NAME + '_cvscores' + ('_w' + str(SMOOTHING_WINDOW) if SMOOTHING_WINDOW>1 else ''),SAVED_VAR_DIR)
    preds=best_net.score(X.reshape((-1, 1, X.shape[1])).astype(np.float32), y.astype(np.uint8) if not '_reg' in DATASET else y.astype(np.float32))
    log_file.write('Accuracy train set: ' + str(clf.best_score_) + '\n')

else:
  if not LOAD_NETWORK:
    best_net = net1.fit(X.reshape((-1, 1, X.shape[1])), y.astype(np.uint8))
    saveVar(best_net,NETWORK_NAME + ('_w' + str(SMOOTHING_WINDOW) if SMOOTHING_WINDOW>1 else ''),SAVED_VAR_DIR)
  preds = best_net.predict(X.reshape((-1, 1, X.shape[1])))
  print(classification_report(y.astype(np.uint8), preds))
  cm = confusion_matrix(y.astype(np.uint8), preds)
  print(cm)
  print("Accuracy:\t%.4f") % ( accuracy_score(y.astype(np.uint8), preds))
  log_file.write('Accuracy train set: ' + str(accuracy_score(y.astype(np.uint8), preds)) + '\n')
  if not NO_TEST_SET:
    preds = best_net.predict(X_test.reshape((-1, 1, X_test.shape[1])))
    print(classification_report(y_test.astype(np.uint8), preds))
    print("Accuracy test:\t%.4f") % (accuracy_score(y_test.astype(np.uint8), preds))
    
    log_file.write('Accuracy test set: ' + str(accuracy_score(y_test.astype(np.uint8), preds)) + '\n')
    
  # Visualize the confusion matrix in case of a test set
    preds = best_net.predict(X_test.reshape((-1, 1, X_test.shape[1])))
    cm = confusion_matrix(y_test.astype(np.uint8), preds)
    print(cm)
log_file.close()