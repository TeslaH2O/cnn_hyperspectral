#!/bin/bash

# This script replicates the some results of the paper.
# This results are obtained using a reduced train set 
# (maximum 1% samples for each class)
DATASETS=( PAVIA PAVIAU KSC PINE SALINA )
#Seeds for shuffling the datasets used to produce the saved model
SEEDS=( 3072 16127 23055 3072 23055 )

for i in `seq 0 4`; do 
    DATASET=${DATASETS[$i]}
    SEED=${SEEDS[$i]}
    echo "Dataset: ${DATASET}"
    python cnn/cnn.py --dataset=${DATASET} --dataset_seed=${SEED} --load_best_network --balance=al0.01 | grep "Accuracy test" | awk -F " " '{print $NF}'

done

